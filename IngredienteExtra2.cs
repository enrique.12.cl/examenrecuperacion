class IngredienteExtra2 : Decorador
{
    public IngredienteExtra2 (OrdenCompra ordenCompra) : base(ordenCompra)
    {
    }
        //Metodo para calcular Precio del Ingrediente Extra

    public override double CalcularPrecioTotal()
    {
        return base.CalcularPrecioTotal() + 1.75;
    }
}