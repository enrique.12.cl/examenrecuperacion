class IngredienteExtra3 : Decorador
{
    public IngredienteExtra3 (OrdenCompra ordenCompra) : base(ordenCompra)
    {
    }
        //Metodo para calcular Precio del Ingrediente Extra

    public override double CalcularPrecioTotal()
    {
        return base.CalcularPrecioTotal() + 0.50;
    }
}