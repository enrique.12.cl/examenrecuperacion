﻿class program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("");
        var taco1 = new Tacos();
        Console.WriteLine($"El precio del taco de pollo es de:" +taco1.CalcularPrecioTotal());
        var IngredienteExtra1 = new IngredienteExtra1(taco1);
        Console.WriteLine($"El precio del taco sin chile es de:" + IngredienteExtra1.CalcularPrecioTotal());
        var IngredienteExtra2 = new IngredienteExtra2(taco1);
        Console.WriteLine($"El precio del taco con chile es de:" + IngredienteExtra2.CalcularPrecioTotal());
        var IngredienteExtra3 = new IngredienteExtra3(taco1);
        Console.WriteLine($"El precio del taco de carne es de:" + IngredienteExtra3.CalcularPrecioTotal());
        var IngredienteExtra4 = new IngredienteExtra1(taco1);
        Console.WriteLine($"El precio del taco de pollo es de:" + IngredienteExtra4.CalcularPrecioTotal());
        Console.WriteLine("");

    }
}