class Decorador : OrdenCompra
{
    protected OrdenCompra ordenCompra;
    public Decorador (OrdenCompra ordenCompra)
    {
        this.ordenCompra=ordenCompra;
    }
    //metodo para calcular el precio del decorador
    public virtual double CalcularPrecioTotal()
    {
        Console.WriteLine ("El precio del decorador es");
        return ordenCompra.CalcularPrecioTotal();
    }
}