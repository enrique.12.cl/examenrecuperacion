class IngredienteExtra4 : Decorador
{
    public IngredienteExtra4 (OrdenCompra ordenCompra) : base(ordenCompra)
    {
    }
    //Metodo para calcular Precio del Ingrediente Extra
    public override double CalcularPrecioTotal()
    {
        return base.CalcularPrecioTotal() + 2.99;
    }
}